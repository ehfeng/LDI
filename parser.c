#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "parser.h"

cons_cell* parse(char* input, int size) {
    if (input[0] == NULL) {
        assert(size == 0);
        return NULL;
    }
    if (input[0] == '(') {
        // Nested case
        int count_bracket = 1;
        int it = 0;
        // Find where the brackets enclose
        while (count_bracket > 0) {
            it++;
            if (input[it] == '(') {
                count_bracket++;
            } else if (input[it] == ')') {
                count_bracket--;
            }
        }
        char buffer[it];
        strncpy(buffer, &input[1], it - 1);
        buffer[it - 1] = '\0';

        // Recurse on what is enclosed by the brackets
        cons_cell* car_cons = parse(buffer, it - 1);

        // Copy everything after the brackets
        int next_char = it + 1;
        while (input[next_char] == ' ' && input[next_char] != NULL) {
            next_char++;
        }
        char buffer_next[500];
        strncpy(buffer_next, &input[next_char], size - next_char + 1);

        // Recurse on everything after the brackets
        cons_cell* cdr_cons = parse(buffer_next, size - next_char);

        return create_cons_cell(car_cons, cdr_cons);
    } else {
        // Not nested case

        // Copy the first term
		int first_space_idx = 0;
	    while (input[first_space_idx] != ' ' && input[first_space_idx] != NULL) {
			first_space_idx++;
        }	
		char buff_first[500];
		strncpy(buff_first, input, first_space_idx);
        buff_first[first_space_idx] = '\0';

        // Create cons_cell for first term
		cons_cell* car_cons = create_cons_cell_w_atom(buff_first, NULL);
		char buff_other[500];
		int next_char = first_space_idx;
	    while (input[next_char] == ' ' && input[next_char] != NULL) {
            next_char++;
        }

        // Copy everything after the first term
		strncpy(buff_other, &input[next_char], size - next_char + 1);
        // Recurse on everything after the first term
		cons_cell* cdr_cons = parse(buff_other, size - next_char);
		car_cons->cdr = cdr_cons;
		return car_cons;
	}
}

int main() {
	char str[500];
	printf("Enter the json-string to parse: \n");
	scanf("%[^\n]s", str);

	printf("You entered: %s\n", str);
    int size = 0;
    while (str[size] != NULL) {
        size++;
    }
    cons_cell* cons = parse(str, size);
    print_cons_tree(cons);
    return 0;
}
