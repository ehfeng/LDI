%{
	#include "util_new.h"
%}

%define api.value.type {cons_cell *}
%token IMPLIES OR AND NOT LE GE LT GT NE HAS MAX MIN AS ASC DESC MOD ASSIGN EQ STAR COMMA DOT

%token SIZE SELECTIVITY OVERLAP
%token FREQUENCY UNIT TIME SPACE

%token IDENTIFIER CONSTANT STRING_LITERAL SIZEOF

%token STORE STORING DYNAMIC STATIC OF TYPE ORDERED BY
%token INDEX LIST ARRAY BINARY TREE DISTRIBUTED POINTER

%token SCHEMA  CLASS ISA PROPERTIES CONSTRAINTS PROPERTY
%token ON DETERMINED COVER QUERY GIVEN FROM SELECT WHERE ORDER
%token PRECOMPUTED ONE EXIST FOR ALL TRANSACTION INTCLASS STRCLASS
%token INTEGER REAL DOUBLEREAL STRING MAXLEN RANGE TO
%token INSERT END CHANGE DELETE DECLARE RETURN UNION UNIONALL
%token LIMIT DISTINCT

%start SQLPProgram
%%
SQLPProgram
    : Query
	{
		printf("Input Query\n");
		cons_cell *n = $1;
		printf("Printing Tree\n");
		print_cons_tree(n);
	}
    ;

Query
	: UnionQuery
	{
		printf("Query 1\n");
		$$ = $1;
	}
	| SelectQuery
	{
		printf("Query 3\n");
		$$ = $1;
	}
	| SelectQuery LIMIT Integer
	{
		printf("Query 4\n");
		$$ = create_limit_operator($1, $3);
	}
	;

SelectQuery
	: SELECT
	{
		printf("SelectQuery 1 \n");
	}
	| SELECT SelectList
	{
		printf("SelectQuery 2 \n");
		$$ = create_rename_operator($2, NULL);
	}
	| SELECT Body
	{
		printf("SelectQuery 3 \n");
		$$ = create_rename_operator($2, NULL);
	}
	| SELECT SelectList Body
	{
		printf("SelectQuery 4 \n");
		$$ = create_proj_operator($2, $3);
	}
	| SELECT DISTINCT Body
	{
		printf("SelectList 5 \n");
		$$ = create_rename_operator($3, NULL);
	}
	| SELECT DISTINCT SelectList Body
	{
		printf("SelectList 6 \n");
		$$ = create_proj_operator($2, $3);
	}
	;

Body
	: FROM TableList
	{
		printf("Body 1 \n");
		$$ = create_rename_operator($2, NULL);
	}
	| FROM WHERE Pred
	{
		printf("Body 2 \n");
		$$ = create_rename_operator($3, NULL);

	}
	| FROM TableList WHERE Pred
	{
		printf("Body 3 \n");
		$$ = create_eval_operator($4, $2);
	}
	;

TableList
   	: TableIdentifier VarIdentifier
	{
		printf("TableList 1\n");
        $$ = create_rename_operator($1, $2);
    }
   	| TableIdentifier VarIdentifier ',' TableList
	{
		printf("TableList 2\n");
    	$$ = create_cross_operator(create_rename_operator($1, $2), $4);
    }
   	;


UnionQuery
	: '(' SelectQuery ')' UNION UnionQuery
	{
		printf("UnionQuery 1\n");
   		$$ = create_union_all_operator($2, $5);
    }
	| '(' SelectQuery ')' UNIONALL UnionQuery
	{
		printf("UnionQuery 2\n");
   		$$ = create_union_all_operator($2, $5);
	}
	| '(' SelectQuery ')'
	{
		printf("UnionQuery 3\n");
		$$ = create_rename_operator($1, NULL);
	}
   	;

SelectList
	: Col
	{
		printf("SelectList 1\n");
		$$ = create_rename_operator($1, NULL);
	}
	| Col AS AttrIdentifier
	{
		printf("SelectList 2\n");
		$$ = create_as_operator($1, $3);
	}
	| Col ',' SelectList
	{
		printf("SelectList 3\n");
		$$ = create_rename_operator($1, $2);
	}
	| Col AS AttrIdentifier ',' SelectList
	{
		printf("SelectList 4\n");
		$$ = create_rename_operator($1, create_as_operator($3, $5));
	}
	;

Col
	: VarIdentifier '.' AttrPath
	{
        printf("col\n");
        $$ = create_spcol($1, $3);
    }

AttrPath
	: AttrIdentifier
	{
		printf("AttrPath 1\n");
		$$ = create_pf($1, NULL);
	}
	| AttrIdentifier '.' AttrPath
	{
		printf("AttrPath 2\n");
		$$ = create_pf($1, $3);
	}

VarIdentifier
	: IDENTIFIER
	{
		printf("VarIdentifier is |%s| ", yytext);
		$$ = create_var(yytext);
	}


TableIdentifier
	: IDENTIFIER
	{
		printf("TableIdentifier is |%s| ", yytext);
		$$ = create_var(yytext);
	}

AttrIdentifier
	: IDENTIFIER
	{
		printf("AttrIdentifier is |%s| ", yytext);
		$$ = create_var(yytext);
	}

CompOperator
	: EQ
	{
		printf("CompOperator EQ\n");
		$$ = create_op(yytext);
	}
	| NE
	{
		printf("CompOperator NE\n");
		$$ = create_op(yytext);
	}
	| LE
	{
		printf("CompOperator LE\n");
		$$ = create_op(yytext);
	}
	| GE
	{
		printf("CompOperator GE\n");
		$$ = create_op(yytext);
	}
	| LT
	{
		printf("CompOperator LT\n");
		$$ = create_op(yytext);
	}
	| GT
	{
		printf("CompOperator GT\n");
		$$ = create_op(yytext);
	}
	;

Pred
	: Conj
	{
		printf("Pred 1\n");
		$$ = $1;
	}
	| Conj OR Pred
	{
		printf("Pred 2 \n");
		$$ = create_or_operator($1, $3);
	}


Conj
	: BasicPred
	{
		printf("Conj 1 \n");
		$$ = $1;
	}
	| BasicPred AND Conj
	{
		printf("Conj 2 \n");
		$$ = create_and_operator($1, $3);
	}

BasicPred
	: Term CompOperator Term
	{
		printf("BasicPred 1\n");
		cons_cell* first_term = create_term($1);
		cons_cell* second_term = create_term($3);
		$$ = create_comp_operator($2, first_term, second_term);
	}
	| EXIST '(' Body ')'
	{
		printf("BasicPred 2\n");
		$$ = create_exist_operator($2);
	}
	| NOT BasicPred
	{
		printf("BasicPred 3 \n");
		$$ = create_not_operator($2);
	}
	| '(' Pred ')'
	{
		printf("BasicPred 4\n");
		$$ = $1;
	}

Term
	: Constant
	{
		printf("Term 1\n");
		$$ = $1;
	}
	| Col
	{
		printf("Term 2\n");
		$$ = $1;
	}


Integer
	: INTEGER
	{
		printf("INTEGER is |%s|", yytext);
		$$ = create_constant(yytext);
	}


Constant
	: INTEGER
	{
		printf("INTEGER is |%s|", yytext);
		$$ = create_constant(yytext);
	}
	| REAL
	{
		printf("REAL is |%s|", yytext);
		$$ = create_constant(yytext);
	}
	| STRING
	{
		printf("STRING is |%s|", yytext);
		$$ = create_constant(yytext);
	}
	;

