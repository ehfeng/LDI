/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_SQLPGRAMMAR_NEW_TAB_H_INCLUDED
# define YY_YY_SQLPGRAMMAR_NEW_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    IMPLIES = 258,
    OR = 259,
    AND = 260,
    NOT = 261,
    LE = 262,
    GE = 263,
    LT = 264,
    GT = 265,
    NE = 266,
    HAS = 267,
    MAX = 268,
    MIN = 269,
    AS = 270,
    ASC = 271,
    DESC = 272,
    MOD = 273,
    ASSIGN = 274,
    EQ = 275,
    STAR = 276,
    COMMA = 277,
    DOT = 278,
    SIZE = 279,
    SELECTIVITY = 280,
    OVERLAP = 281,
    FREQUENCY = 282,
    UNIT = 283,
    TIME = 284,
    SPACE = 285,
    IDENTIFIER = 286,
    CONSTANT = 287,
    STRING_LITERAL = 288,
    SIZEOF = 289,
    STORE = 290,
    STORING = 291,
    DYNAMIC = 292,
    STATIC = 293,
    OF = 294,
    TYPE = 295,
    ORDERED = 296,
    BY = 297,
    INDEX = 298,
    LIST = 299,
    ARRAY = 300,
    BINARY = 301,
    TREE = 302,
    DISTRIBUTED = 303,
    POINTER = 304,
    SCHEMA = 305,
    CLASS = 306,
    ISA = 307,
    PROPERTIES = 308,
    CONSTRAINTS = 309,
    PROPERTY = 310,
    ON = 311,
    DETERMINED = 312,
    COVER = 313,
    QUERY = 314,
    GIVEN = 315,
    FROM = 316,
    SELECT = 317,
    WHERE = 318,
    ORDER = 319,
    PRECOMPUTED = 320,
    ONE = 321,
    EXIST = 322,
    FOR = 323,
    ALL = 324,
    TRANSACTION = 325,
    INTCLASS = 326,
    STRCLASS = 327,
    INTEGER = 328,
    REAL = 329,
    DOUBLEREAL = 330,
    STRING = 331,
    MAXLEN = 332,
    RANGE = 333,
    TO = 334,
    INSERT = 335,
    END = 336,
    CHANGE = 337,
    DELETE = 338,
    DECLARE = 339,
    RETURN = 340,
    UNION = 341,
    UNIONALL = 342,
    LIMIT = 343,
    DISTINCT = 344
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef cons_cell * YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_SQLPGRAMMAR_NEW_TAB_H_INCLUDED  */
