#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

// Basic building blocks that contain char values
typedef struct atom {
	char* val;
} atom;

// A linked list like structure
// car points to element the node is holding
// cdr points to next cons cell in list or null
// is_atom determines if car is an atom
typedef struct cons_cell {
    void* car;
    struct cons_cell* cdr;
	bool is_atom;
} cons_cell;

// Creates a cons_cell
cons_cell* create_cons_cell(void* car, cons_cell* cdr);
// Creates an atom
atom* create_atom(char* val);
// Creates a cons_cell that has an atom as its car
cons_cell* create_cons_cell_w_atom(char* val, cons_cell* cdr);

/*

Below comment block are functions that implement the following relational
algebra expressions for SQL queries:

<ra> := (comp <op> <term> <term> <ra>)
        | (atom <table> <var>)
        | (union-all <ra> <ra>)
        | (cross <ra> <ra>)
        | (proj ((rename <col> <col>)) <ra>)
        | (not <ra> <ra>)
        | (elim <ra>)
        | (limit n <ra> <ra>)
        | (eval <ra> <ra>)

<term> := <col> | <const>
<col> := <spcol> | <scol>
<spcol> := <var> "." <pf>   // for SQLP
<scol> := <var> "." <attr>  // for SQL
<pf> := <attr> <attr>
<var> := identifier
<attr> := identifier
<table> := identifier
<term> := identifier
<const> := identifier
<op> := [GT, GE, EQ, LE, LT]

*/

// The following are all helper functions to create the
// various relational algebra expressions
cons_cell* create_spcol(cons_cell* var, cons_cell* pf);
cons_cell* create_pf(cons_cell* attr, cons_cell* next_attr);
cons_cell* create_table(char *table);
cons_cell* create_term(cons_cell *term);
cons_cell* create_constant(char* constant);
cons_cell* create_col(char *col);
cons_cell* create_attr(char *attr);
cons_cell* create_var(char *var);
cons_cell* create_op(char *op);
cons_cell* create_comp_operator(cons_cell* op, cons_cell* term1, cons_cell* term2, cons_cell* ra);
cons_cell* create_atom_operator(cons_cell* table, cons_cell* var);
cons_cell* create_union_all_operator(cons_cell* ra1, cons_cell* ra2);
cons_cell* create_union_operator(cons_cell* ra1, cons_cell* ra2);
cons_cell* create_cross_operator(cons_cell* ra1, cons_cell* ra2);
cons_cell* create_rename_operator(cons_cell* table, cons_cell* var);
cons_cell* create_proj_operator(cons_cell* assign, cons_cell* ra);
cons_cell* create_not_operator(cons_cell* ra1, cons_cell* ra2);
cons_cell* create_exist_operator(cons_cell* ra1);
cons_cell* create_limit_operator(cons_cell* ra1, cons_cell* ra2);
cons_cell* create_elim_operator(cons_cell* ra1);
cons_cell* create_eval_operator(cons_cell* logic, cons_cell* cross);

// Prints an in order traversal of the tree
void print_cons_tree(cons_cell *root);

void print_cons_tree_helper(cons_cell *root, int indent);
#endif



