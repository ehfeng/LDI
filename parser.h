#ifndef PARSER_H
#define PARSER_H

#include "util.h"

cons_cell* parse(char* input, int size);

#endif